from PIL import Image, ImageDraw, ImageFont
import math
from copy import deepcopy as dc

# constants
PPI = 600
DIMENSIONS = (4961, 7016) # A4 at 600 PPI
LINE_WIDTH_IN = 0.004
COLUMN_WIDTH_IN = 0.3
ROW_HEIGHT_IN = 0.3 # should be same as COLUMN_WIDTH
NUM_GHOST_COLUMNS = 6 # Number of greyed out honzi. 0 for all. Multiplied for multi-hanzi words.
MARGINS_IN = [1,1.166]
HANZI_TOP_OFFSET_IN = -0.02
LATIN_LEFT_OFFSET_IN = 0.02

HANZI_TOP_OFFSET_PIX = round(HANZI_TOP_OFFSET_IN*PPI)
LATIN_LEFT_OFFSET_PIX = round(LATIN_LEFT_OFFSET_IN*PPI)
MARGINS_PIX = [round(MARGINS_IN[0]*PPI),round(MARGINS_IN[1]*PPI)]
COLUMN_WIDTH_PIX = round(COLUMN_WIDTH_IN*PPI)
ROW_HEIGHT_PIX = round(ROW_HEIGHT_IN*PPI)
LINE_WIDTH_PIX = round(LINE_WIDTH_IN*PPI)
HANZI_FONT_SIZE = min(COLUMN_WIDTH_PIX, ROW_HEIGHT_PIX) # min(COLUMN_WIDTH, ROW_HEIGHT)
LATIN_FONT_SIZE = COLUMN_WIDTH_PIX//5
COLUMNS = math.floor((DIMENSIONS[0]-MARGINS_PIX[0]*2)/COLUMN_WIDTH_PIX)
ROWS = math.floor((DIMENSIONS[1]-MARGINS_PIX[1]*2)/ROW_HEIGHT_PIX)
MM_IN_PIXELS = round(PPI/25.4) # PPI = per inch so for milimetre is /25.4 (1in=25.4mm)

output_images = list()
ukai = ImageFont.truetype('/usr/share/fonts/TTF/ukai.ttc', HANZI_FONT_SIZE)
notosans = ImageFont.truetype('/usr/share/fonts/noto/NotoSans-Regular.ttf', LATIN_FONT_SIZE)

words = [['Phone\ndin6waa2', '電話'], ['hear\nteng1', '聽'], ['cat\nmaau1', '貓'], ['football\nzuk1kau4', '足球'], ['Music\njam1ngok6', '音樂'], ['live\nzyu6', '住'], ['In that case, so\nso2ji5', '所以'], ['write\nse2', '寫'], ['restaurant\ncaan1teng1', '餐廳'], ['tennis\nmong5kau4', '網球'], ['north\nbak1', '北'], ['east\ndung1', '東'], ['south\nnaam4', '南'], ['west\nsai1', '西'], ['learn\nhok6caa4', '學習'], ['hotel\nzau2dim3', '酒店'], ['To lose\nwai6sat1', '遺失'], ['Interesting\njau5ceoi3', '有趣'], ['Super\nciu4kap6', '超級'], ['Market\nsi4coeng6', '巿場'], ['Zoo\ndung6 mat6 jyun4', '動物園'], ['Tourist\njau5haak3', '遊客'], ['nearby\nfu4gan4', '附近'], ['area (as in neighbourhood)\ndei6keoi1', '地區'], ['Dear...\ncin1ngoi3', '親愛'], ['Manager\nging1lei5', '經理'], ['Taiwan\ntoi1waan4', '臺灣'], ['Food\nsik6mat6', '食物'], ['At\nzoi6', '在'], ['Will\nwui5', '會'], ['and\nwo4', '和'], ['Weekend\nlai5baai3', '禮拜'], ['Tomorrow\nming4tin1', '明天'], ['Sossij\ncoeng4zai2', '腸仔'], ['Explain\ngaai2sik1', '解釋'], ['Country\ngwok3', '國'], ['Come\nloi4, lai4, lei4', '來'], ['history\nlik6si2', '歷史'], ['Night\nmaan4soeng6', '晚上'], ['Together\njat1tung4', '一同'], ['Rubber (stationary+material)\nzoeng6pei4', '橡皮(擦)'], ['Ruler\ncek3', '尺(子)'], ['Spring\ncoen1tin1', '春天'], ['Summer\nhaa6 tin1', '夏天'], ['autumn\ncau1 tin1', '秋天'], ['Winter\ndung1tin1', '冬天'], ['Kitchen\ncyu4 fong4', '廚房'], ['Dining room\nfaan6teng1', '飯聽'], ['Stairs\nlau4tai1', '樓梯'], ['Garden \nfaa1jyun2', '花園'], ['Bedroom\nseoi6fong4', '睡房'], ['Floor x\ndei6 x lau4', '第？樓'], ['Enormous\ngeoi6daai6', '巨大'], ['Living room \nhaak3teng1', '客聽'], ['Comfortable \nsyu3fuk3', '舒服'], ['Modern\njin3doi3', '現代'], ['石\n', '右'], ['種\n鐘', '鍾'], ['Experience (v.)\ntai3jim6', '體驗'], ['to wear\ncyun1', '穿'], ['Spacious and well-lit\nOpen-minded\nhoi1long5', '開朗'], ['Cured pork (e.g. bacon)\nlaap6juk6', '臘肉'], ['Mahjong \nmaa4zoeng3', '麻雀'], ['Bird\nniu5', '鳥'], ['Bowling alley\nbou2 ling4 kau4 coeng4', '保齡球場'], ['apartment\ngung1jyu6', '公寓'], ['Farm\nnung4coeng4', '農場'], ['X is better than Y\nx bei2 y gang3hou2', 'x比y更好'], ['X is worse than y\nx bei2 y gang3 waai6', 'x比y更壞'], ['Bad\nwaai6', '壞'], ['Bowling\nbou2 ling4 kau4', '保齡球'], ['More\nbei2', '比'], ['x比y_好\ngang3', '更'], ['closet\nji1 gwai6', '衣櫃'], ['bathtub\njuk6 gong1\n', '浴缸'], ['bed\ncong4', '床'], ['not have\nmut6 jau5', '沒有'], ['That\nnaa5', '那'], ['which\nnaa5', '哪'], ['Place\nleoi5', '裡'], ['This\nze2', '這'], ['Baseball\npaang5 kau4', '棒球'], ['Basketball\nlaam4kau4', '籃球'], ['Climbing\npaan1sek6', '攀石'], ['To climb\npaan1', '攀'], ['Flower\nfaa1', '花'], ['Well-known song (a classic)\nming4kuk1', '名曲'], ['To be clean\ngon1zeng6', '乾淨'], ['Detergent\nsai2ji1fan2', '洗衣粉'], ['Washing line\nlong6ji1sing4', '晾衣繩'], ['Husband and wife, married couple\nfu1fu5', '夫婦'], ['To move sth\nbun1', '搬'], ['classifier for households\nfu4', '户'], ['neighbor\nloen4goei1', '鄰居'], ['Young\nnin4heng1', '年輕'], ['Wife\ncai1zi2', '妻子'], ['Currently\nzing3zoi6', '正在'], ['outside, out\nngoi6min6', '外面'], ['wash, clean\nsai2', '洗'], ['Husband\nzoeng6fu1', '丈夫'], ['To state (a fact)\nsyut3 dou6', '說道'], ['maybe\njaa5heoi2', '也許'], ['how (adverb)\njyu4ho4', '如何'], ['roughly, about\ndaai6koi3', '大概'], ['cellar\ndei6haa6sat1', '地下室'], ['To be afraid\nhoi6paa3', '害怕'], ['Later\nhau6loi4', '後來'], ['Also\njau6', '又'], ['Full\nmun5', '滿'], ['This much\nze2mo1', '這麼'], ['Room\nfong4gaan1', '房間'], ['Corn, grain, cereal\nguk1', '穀'], ['Often\nsoeng4soeng4', '常常'], ['Competition, match \nbei2coi3', '比賽'], ['"\nSweet-sour pork"\ngu1lou1juk6', '咕嚕肉'], ['"\nRoast Goose"\nsui1ngo4', '燒鵝'], ['Domestic Goose\nngo4', '鵝'], ['Wild goose\nngaan6', '雁'], ['"\nChickens\' feet (Phoenix talons)"\nfung6zaau2', '鳳爪'], ['Male phoenix\nfung6', '鳳'], ['Female phoenix\nwong4', '凰'], ['凰\n', '鳳'], ['Dragon\nlung4', '龍'], ['give\nkap1', '給'], ['"\nMilk tea (traditional Hong Kong recipe)"\ncaa4zau2', '茶走'], ['"\nEgg tart (like the one Cedric\'s mum gave us)"\ndaan6taat3', '蛋撻'], ['"Beef Brisket (belly beef) noodles\n"\nngau4naam5min3', '牛腩面'], ['"\nBalls of fish."\njyu4 daan6', '魚蛋'], ['"\nSteamed Shrimp Dumplings"\nhaa1 gaau2', '蝦餃'], ['Yue cuisine\njyut3coi3', '粵菜'], ['Soy sauce chicken\nsi6jau4gai1', '豉油雞'], ['Biscuit\nbeng5gon3', '餅乾'], ['When x, y\n"x di1 si4 hau6, zau4 y"', '"x的時候，就y"']]


def new_page():
  page = Image.new("L", DIMENSIONS, 255)
  page_editable = ImageDraw.Draw(page)

  # image grid
  for i in range(0,1+COLUMNS):
    page_editable.line(
      (
        (MARGINS_PIX[0]+i*COLUMN_WIDTH_PIX,MARGINS_PIX[1]),(MARGINS_PIX[0]+i*COLUMN_WIDTH_PIX,DIMENSIONS[1]-MARGINS_PIX[1])
      ),fill=0,width=LINE_WIDTH_PIX
    )
  for i in range(0,1+ROWS):
    page_editable.line(
      (
        (MARGINS_PIX[0],MARGINS_PIX[1]+i*ROW_HEIGHT_PIX),(DIMENSIONS[0]-MARGINS_PIX[0],MARGINS_PIX[1]+i*ROW_HEIGHT_PIX)
      ),fill=0,width=LINE_WIDTH_PIX
    )
  
  return page

def save_page(page, pagenum):
  page.save("page "+str(pagenum)+".pdf", dpi=(PPI, PPI))


row = 0
pages = 0
page = new_page()
page_editable = ImageDraw.Draw(page)
for wordnum, word in enumerate(words):
  # Must be in format [English, Hanzi]
  assert(len(word)==2)
  assert(len(word[1])>0)
  page_editable.text((LATIN_LEFT_OFFSET_PIX+MARGINS_PIX[0], MARGINS_PIX[1]+row*ROW_HEIGHT_PIX), word[0], fill=0, font=notosans)
  for character in word[1]:
    # print the character in black
    page_editable.text((COLUMN_WIDTH_PIX+MARGINS_PIX[0], HANZI_TOP_OFFSET_PIX+row*ROW_HEIGHT_PIX+MARGINS_PIX[1]), character, fill=0, font=ukai)
    # print greyed-out versions of the character
    for i in range(2,COLUMNS if NUM_GHOST_COLUMNS == 0 else min(NUM_GHOST_COLUMNS+2, COLUMNS)):
      page_editable.text((i*COLUMN_WIDTH_PIX+MARGINS_PIX[0], HANZI_TOP_OFFSET_PIX+row*ROW_HEIGHT_PIX+MARGINS_PIX[1]), character, fill=195, font=ukai)
    
    row += 1
  
    if row >= ROWS: # >= because row is 0-indexed but ROWS is not
      pages += 1
      save_page(page, pages)
      page = new_page()
      page_editable = ImageDraw.Draw(page)
      row = 0

  print(str(100*wordnum/len(words))+"% complete")

pages += 1
save_page(page,pages)