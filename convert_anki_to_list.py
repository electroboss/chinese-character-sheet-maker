docopy = input("Copy to clipboard? y/n")
while docopy not in ["y","n"]:
  docopy = input("Copy to clipboard? y/n")
if docopy == "y":
  from pyperclip import copy as c

infile = open("Cantonese written and common.txt","r")
intext = infile.read()

out = [[x.split("\t")[1]+"\n"+x.split("\t")[2],x.split("\t")[0]] for x in intext.split("\n")]

out = out.replace("<br>","\\n")
print(out)

if docopy == "y":
  c(str(out))